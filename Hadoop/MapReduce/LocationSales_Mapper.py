#! /usr/bin/env python
import sys


# read standard input line by line
for line in sys.stdin:
    # Extract the needed information
    fields = line.strip().split("\t")
    location = fields[2]
    sales = fields[4]

    # Print out the intermediate record as "key \t value"
    print >> sys.stdout, "%s\t%s" % (location, sales)