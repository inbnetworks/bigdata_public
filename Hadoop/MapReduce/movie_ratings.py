from mrjob.job import MRJob
from mrjob.step import MRStep

class MovieRatings(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_extractRatings,
                   reducer=self.reducer_getRatings)
        ]

    def mapper_extractRatings(self, _, line):
        (userID, movieID, rating, timestamp) = line.split('::')
        yield rating, 1

    def reducer_getRatings(self, key, values):
        yield key, sum(values)

if __name__ == '__main__':
    MovieRatings.run()